import Vue from "vue";
import VueRouter from "vue-router";
import Login from "@/views/Login";
import Home from "@/views/Home";
import { auth } from "@/firebase";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login",
    component: Login
  },
  {
    path: "/home",
    component: Home,
    children: [
      {
        path: "profile",
        name: "profile",
        component: () =>
          import(/* webpackChunkName: "profile" */ "@/views/Profile")
      },
      {
        // default route
        path: "",
        component: () =>
          import(/* webpackChunkName: "profile" */ "@/views/Profile")
      },
      {
        path: "application",
        name: "application",
        component: () =>
          import(/* webpackChunkName: "application" */ "@/views/Application")
      },
      {
        path: "info",
        name: "info",
        component: () => import(/* webpackChunkName: "info" */ "@/views/Info")
      },
      {
        path: "license",
        name: "license",
        component: () =>
          import(/* webpackChunkName: "license" */ "@/views/License")
      }
    ]
  },
  {
    path: "*",
    redirect: "/home/profile"
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

// check authentication
router.beforeResolve((to, from, next) => {
  console.log("to", to);
  auth.onAuthStateChanged(user => {
    if (user) {
      console.log("user", user);
      if (to.path === "/") {
        next({ path: "/home/profile" });
      } else {
        next();
      }
    } else {
      if (to.path === "/") {
        next();
      } else {
        next({ path: "/" });
      }
    }
  });
});

export default router;
