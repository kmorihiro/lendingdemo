import Vue from "vue";
import App from "@/App";
import router from "@/router";
import "@fortawesome/fontawesome-free/js/fontawesome";
import "@fortawesome/fontawesome-free/js/solid";
import "@fortawesome/fontawesome-free/js/regular";
import "@/assets/global.css";
import { rtdbPlugin } from "vuefire";

Vue.config.productionTip = false;

Vue.use(rtdbPlugin);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
