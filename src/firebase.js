import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/functions";

const config = {
  apiKey: "AIzaSyCLc2EhXfsdLWzY3i6bWu8Vkmr3RoxfhOM",
  authDomain: "lendingdemo-dr.firebaseapp.com",
  databaseURL: "https://lendingdemo-dr.firebaseio.com",
  messagingSenderId: "953296472112",
  projectId: "lendingdemo-dr",
  storageBucket: "lendingdemo-dr.appspot.com"
};
firebase.initializeApp(config);

export const auth = firebase.auth();
export const db = firebase.database();
export const functions = firebase.functions();

export default firebase;
