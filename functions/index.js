const functions = require("firebase-functions");
const admin = require("firebase-admin");
const rp = require("request-promise");

admin.initializeApp();
const db = admin.database();

const datarobot = functions.config().datarobot;
const headers = {
  "Content-Type": "application/json",
  "datarobot-key": datarobot.datarobot_key
};

exports.predict = functions.https.onCall(async (data, context) => {
  console.log("data", data);
  if (!context.auth)
    throw new functions.https.HttpsError(
      "unauthenticated",
      "You must be authenticated."
    );
  if (!data) {
    throw new functions.https.HttpsError(
      "failed-precondition",
      "Request data is invalid."
    );
  }
  // request for DataRobot v1 API
  const drReq = {
    loan_amnt: null,
    grade: null,
    sub_grade: null,
    emp_title: null,
    emp_length: null,
    home_ownership: null,
    annual_inc: null,
    purpose: null,
    title: null,
    zip_code: null,
    addr_state: null,
    dti: null,
    delinq_2yrs: null,
    earliest_cr_line: null,
    inq_last_6mths: null,
    mths_since_last_delinq: null,
    mths_since_last_record: null,
    open_acc: null,
    pub_rec: null,
    revol_bal: null,
    revol_util: null,
    total_acc: null,
    initial_list_status: null,
    collections_12_mths_ex_med: null,
    mths_since_last_major_derog: null,
    application_type: null,
    acc_now_delinq: null,
    tot_coll_amt: null,
    tot_cur_bal: null
  };

  const userRef = db.ref(`users/${data}`);
  const user = await userRef.once("value").catch(e => {
    throw new functions.https.HttpsError("failed-precondition", e);
  });

  for (let i in user.val()) {
    if (i === "displayName" || i === "email") continue;
    drReq[i] = user.val()[i];
  }

  const options = {
    url: `https://${datarobot.endpoint}/predApi/v1.0/deployments/${datarobot.deployment_id}/predictions`,
    method: "POST",
    headers: headers,
    body: JSON.stringify([drReq]),
    auth: {
      user: datarobot.username,
      pass: datarobot.api_key
    }
  };

  return await rp(options);
});
