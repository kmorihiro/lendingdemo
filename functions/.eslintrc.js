module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["eslint:recommended", "prettier"],
  plugins: ["promise"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off"
  },
  parserOptions: {
    // Required for certain syntax usages
    "ecmaVersion": 2017
  }
};
